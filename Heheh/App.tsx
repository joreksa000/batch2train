import {View, Text, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';

const App = () => {
  const [Boxs, setBoxs] = useState(true);
  const [box2, setBox2] = useState(true);
  return (
    <View style={{flex: 1, backgroundColor: Boxs == true ? 'black' : 'white'}}>
      <View
        style={{
          flex: 1,

          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{marginHorizontal: 15}}>
          <TouchableOpacity
            onPress={() => {
              //isi box = true kalau kita
              // isi tidak sama dengan true maka akan isi apa? isi false begitu juga sebalik nya
              setBoxs(!Boxs);
            }}
            style={{
              backgroundColor: Boxs == false ? 'white' : 'blue',
              paddingHorizontal: 20,
              paddingVertical: 10,
              alignItems: 'center',
              elevation: 5,
              borderRadius: 8,
            }}>
            <Text
              style={{
                color: Boxs == false ? 'black' : 'white',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              {Boxs == true ? 'white mode' : 'dark mode'}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{marginHorizontal: 15}}>
          <TouchableOpacity
            onPress={() => {
              setBox2(!box2);
            }}
            style={{
              backgroundColor: box2 == false ? 'white' : 'blue',
              paddingHorizontal: 20,
              paddingVertical: 10,
              alignItems: 'center',
              elevation: 5,
              borderRadius: 8,
            }}>
            <Text
              style={{
                color: box2 == false ? 'blue' : 'white',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Button 2
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{color: 'black', fontSize: 16}}>
          isi si usestate adalah {Boxs ? 'true' : 'false'}
        </Text>
      </View>
    </View>
  );
};

export default App;
