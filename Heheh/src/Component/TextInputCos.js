import { View, Text, TextInput } from 'react-native'
import React, { useState } from 'react'

const TextInputCos = ({
    placeholders,
    onChangeTexts,
    values,
    heights,
}) => {
    const [lokalText, setLokalText] = useState('')
    return (
        <View>
            <View
                style={{
                    height: heights ? heights : 50,
                    borderWidth: 1,
                    borderRadius: 8,
                    marginHorizontal: 10,
                    paddingHorizontal: 10,
                    justifyContent: 'center',
                }}>
                <TextInput
                    placeholder={placeholders}
                    placeholderTextColor={'black'}
                    value={values}
                    onChangeText={Text => {
                        setLokalText(Text)
                        onChangeTexts(Text)
                    }}
                />
            </View>
        </View>
    )
}

export default TextInputCos